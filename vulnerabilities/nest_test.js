
const express = require('express');
const router = express.Router()

const { exec, spawn }  = require('child_process');

router.post('/nest_test', (req,res) => {
    exec(req.body.url, (error) => {
        if (error) {
            return res.send('error');
        }
        res.send('pong')
    });
})

module.exports = router
